asin_config = YAML.load_file(File.join(Rails.root, 'config/asin.yml'))

ASIN::Configuration.configure do |config|
  config.key    = asin_config[Rails.env]['access_key_id']
  config.secret = asin_config[Rails.env]['secret_access_key']
  config.associate_tag = 'foobar'
end
