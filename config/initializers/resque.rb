require 'resque/server'

resque_config = YAML.load_file(File.join(Rails.root, 'config/resque.yml'))
Resque.redis = resque_config[Rails.env]

resque_auth_config = YAML.load_file(File.join(Rails.root, 'config/resque_auth.yml'))
Resque::Server.use(Rack::Auth::Basic) do |user, password|
  user == resque_auth_config['user'] && password == resque_auth_config['password']
end
