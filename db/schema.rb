# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20120911140825) do

  create_table "likes", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "likes", ["name"], :name => "index_likes_on_name", :unique => true

  create_table "likes_products", :id => false, :force => true do |t|
    t.integer "like_id"
    t.integer "product_id"
  end

  add_index "likes_products", ["like_id", "product_id"], :name => "index_likes_products_on_like_id_and_product_id"
  add_index "likes_products", ["product_id", "like_id"], :name => "index_likes_products_on_product_id_and_like_id"

  create_table "products", :force => true do |t|
    t.string   "asin"
    t.text     "json"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "products", ["asin"], :name => "index_products_on_asin", :unique => true

end
