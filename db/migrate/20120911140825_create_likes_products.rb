class CreateLikesProducts < ActiveRecord::Migration
  def change
    create_table :likes_products, :id => false do |t|
      t.references :like
      t.references :product
    end

    add_index :likes_products, [:like_id, :product_id]
    add_index :likes_products, [:product_id, :like_id]
  end
end
