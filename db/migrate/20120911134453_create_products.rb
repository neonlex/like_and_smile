class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :asin
      t.text :json

      t.timestamps
    end

    add_index :products, :asin, :unique => true
  end
end
