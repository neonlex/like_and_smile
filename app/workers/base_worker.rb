class BaseWorker
  def self.perform(*args)
    method = args.delete_at(0)

    instance = new
    instance.send(method.to_sym, *args)
  end

  def logger
    Rails.logger
  end
end
