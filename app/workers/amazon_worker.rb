class AmazonWorker < BaseWorker
  include ASIN::Client

  @queue = :amazon

  def retrieve_products_by_like(query)
    like = Like.find_or_create_by_name(query)

    logger.info "Retrieving products by like: #{like.name}"

    items = search_keywords('Keywords', like.name)

    items.each do |item|
      product = Product.find_or_create_by_asin(item.asin, :json => item.raw.to_json)
      logger.info "Retrieved product with ASIN: #{product.asin}"
    end
  end
end
