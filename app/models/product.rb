class Product < ActiveRecord::Base
  attr_accessible :asin, :json

  has_and_belongs_to_many :likes

  validates :asin, :uniqueness => true
end
